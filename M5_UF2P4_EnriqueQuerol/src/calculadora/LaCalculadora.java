package calculadora;

import java.util.Scanner;

/**
 * <h2>clase Calculadora, se utiliza para hacer operaciones matematicas simples</h2>
  
 * @version 1.01
 * @author Enrique Querol Castro
 * @since 22/02/2022
 */


public class LaCalculadora {
	/**
  	 * Creacion del scanner que nos permetira introducir numeros por pantalla para hacer las operaciones que seleccionemos mas adelante
  	 
  	 */
    static Scanner reader = new Scanner(System.in);
    /**
	* TEXTO DE PRUEBA 
  	 * Main de la aplicacion LaCalculadora 
  	 * @param args Son los orgumentos que se almacenan en el string del main 	 
  	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			LaCalculadora.divideix(15, 0);
			
		} catch(ArithmeticException e) {
			System.out.println("Excepci� capturada: " + e);
		}
		
		
		int op1 = 0, op2 = 0;
		char opcio;
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                        resultat = divisio(op1, op2);
                        if (resultat == -99)
                            System.out.print("No ha fet la divisi�");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci� erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}
//M�todos publicos
	/**
  	 *Funcion muestra errores 
  	 */

    public static void mostrarError() { 
		System.out.println("\nError, cal introduir primer els valors a operar");
	}

//M�todos publicos
	
  	/**
  	 *Funcion que suma dos numeros
  	  *@param a , introduce el primer numero
	 *@param b , introduce el segundo numero
	 *@return res , Resultado de la operacion
  	 
  	 */
	public static int suma(int a, int b) {
		int res;
		res = a + b;
		return res;
	}
	
	/**
	 *Funcion que resta dos numeros
	  *@param a , introduce el primer numero
	 *@param b , introduce el segundo numero
	 *@return res , Resultado de la operacion
	*/
	public static int resta(int a, int b) { 
		int res;
		res = a - b;
		return res;
	}

	/**
	* Funcion que multiplica dos numeros
	 *@param a , introduce el primer numero
	 *@param b , introduce el segundo numero
	 *@return res , Resultado de la operacion
	*/
	public static int multiplicacio(int a, int b) { 
		int res;
		res = a * b;
		return res;
	}

	/**
	 * Funcion que divide dos numeros
	 * @return <ul>
	 *            <li>M :muestra el residuo</li>
	 *            <li>D :muestra el resultado de la division</li>
	 *         </ul>
	  *@param a , introduce el primer numero
	 *@param b , introduce el segundo numero
	 */
	public static int divisio(int a, int b) { 
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci� incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}

	/**
	 *Funcion que lee los caracteres introducidos por el scanner
	 * @return <ul>
	 *             <li>Primero muestra por pantalla que introduzcas un caracter</li>
	 *             <li>Lo introduces por teclado y se guarda en una varaible</li>
	 *        </ul>
	 */
	public static char llegirCar()
	{
		char car;

		System.out.print("Introdueix un car�cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	/**
	 *Funcion que divide 
	 * @return <ul>
	 *           <li>Divide entre 0</li>       
	 *        </ul>
	 *@param num1 , introduce el primer numero
	 *@param num2 , introduce el segundo numero
	 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisi� entre zero");
	else 
	res = num1/num2;
		return res;
	}

	/**
	 *  Funcion que lee los numeros introducidos por el scanner
	 * @return <ul>
	 *              <li>Primero muestra por pantalla que introduzcas un numero</li>
	 *              <li>Lo introduces por teclado y se guarda en una varaible</li>
	 *              <li>En caso de que lo introducido no sea un numero, muestra error</li>
	 *         </ul>
	 */
	public static int llegirEnter() 
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	/**
	 *  Funcion muestra el resultado
	 * @param res, Es el resultado de la operacion
	 */
	public static void visualitzar(int res) { 
		System.out.println("\nEl resultat de l'operacio �s " + res);
	}
	/**
	 *  Funcion donde tenemos las opciones del menu dependiendo de lo que introducimos por consola
	 *  <ul>
	 *             <li>"o" ,Obtenemos los valores</li>
	 *             <li>"+" ,Hace una suma de los valores</li>
	 *             <li>"-" ,Hace una resta de los valores</li>
	 *             <li>"*" ,Hace una multiplicacion de los valores</li>
	 *             <li>"/" ,Hace una division de los valores</li>
	 *             <li>"v" ,podemos visualizar el resultado</li>
	 *             <li>"s" ,Salimos del programa</li>                 
	 * </ul>
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci� i ");
	}

}
